# ASOS Automation for HoneyBook - Ethan Haas

To run all tests, run the command:
**npx cypress open**

this will open the Cypress UI where you can click the **Run 2 integration specs** button

after the tests will run, use the Cypress UI to navigate between the tests and see what actions happened during each test

#### enjoy :-)
