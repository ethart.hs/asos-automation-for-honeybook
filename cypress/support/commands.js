import {
	getRandomInt,
	getRandomProperty,
	valueOfNumberOrRange,
} from "./functions";
import {
	mandatoryFieldsLocators as fields,
	optionalFieldsLocators as optionalFields,
	errors,
} from "../fixtures/locators";

Cypress.Commands.add("selectBirthYear", (day, month, year) => {
	if (day) {
		const dayValue = valueOfNumberOrRange(day);
		cy.get(fields.birthDay).select(dayValue).should("have.value", dayValue);
	}
	if (month) {
		const monthValue = valueOfNumberOrRange(month);
		cy.get(fields.birthMonth)
			.select(monthValue)
			.should("have.value", monthValue);
	}
	if (year) {
		const yearValue = valueOfNumberOrRange(year);
		cy.get(fields.birthYear).select(yearValue).should("have.value", yearValue);
	}
});

Cypress.Commands.add("chooseGenderWear", () => {
	cy.get(getRandomProperty(optionalFields.genderWear))
		.click()
		.should("have.class", "selected");
});

Cypress.Commands.add("checkContactPrefs", () => {
	const amount = getRandomInt(0, 4);

	for (let i = 0; i < amount; i++) {
		cy.get(optionalFields.contactPrefs).eq(i).click();
	}
});

Cypress.Commands.add("validateFieldError", (fieldLocator, errorMessage) => {
	cy.get(fieldLocator).should("have.class", errors.fieldErrorClass);
	cy.get(errors.errorMessage).should("contain", errorMessage);
});

// Cypress 'type' command doesn't accept an empty string
// and passing " " or "{enter}" to mimic an empty field has undesired effects on other elements
Cypress.Commands.add(
	"typeWithEmpty",
	{ prevSubject: "element" },
	(subject, text) => {
		if (text === "") {
			cy.wrap(subject, { log: false }).clear();
		} else {
			cy.wrap(subject, { log: false }).type(text);
		}
	}
);
