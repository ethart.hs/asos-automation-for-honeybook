// get random integer between 2 numbers
// including the numbers themselves
const getRandomInt = (min, max) => {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1) + min);
};

// get random property from an object
const getRandomProperty = (object) => {
	const keys = Object.keys(object);
	return object[keys[(keys.length * Math.random()) << 0]];
};

const valueOfNumberOrRange = (param) => {
	return typeof param === "number"
		? param.toString()
		: getRandomInt(param.min, param.max).toString();
};

export { getRandomInt, getRandomProperty, valueOfNumberOrRange };
