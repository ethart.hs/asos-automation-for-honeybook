/// <reference types= "cypress"/>

import {
	mandatoryFieldsLocators as fields,
	registerButton,
} from "../fixtures/locators";
import { negativeTestInfo as testInfo } from "../fixtures/test-info";

describe("Join Asos Negative Tests", () => {
	beforeEach(() => {
		cy.visit("");
	});

	testInfo.forEach((test) => {
		it(`${test.testName} Negative Test`, () => {
			cy.get(fields.email).typeWithEmpty(test.email);
			cy.get(fields.firstName).typeWithEmpty(test.firstName);
			cy.get(fields.lastName).typeWithEmpty(test.lastName);
			cy.get(fields.password).typeWithEmpty(test.password);

			cy.selectBirthYear(
				test.birthdate.day,
				test.birthdate.month,
				test.birthdate.year
			);

			// the register button is sometimes disabled or enabled for different fields, which is irrelevant to the current test
			cy.get(registerButton).click({ force: true });

			cy.validateFieldError(fields[test.errorField], test.errorMessage);
		});
	});
});
