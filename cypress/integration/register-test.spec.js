/// <reference types= "cypress"/>

import {
	mandatoryFieldsLocators as fields,
	registerButton,
} from "../fixtures/locators";
import { positiveTestInfo as testInfo } from "../fixtures/test-info";

describe("Join Asos Test", () => {
	before(() => {
		cy.visit("");
	});

	// My original plan was to validate a "successful" registration with the error that the email aaddress already exists as a different user
	// But, the automation behaves differently due to security reasons so the validation now is to see the "Access Denied" message (which only appears after a successful registration in the automation)
	it("Registers To Asos", () => {
		cy.get(fields.email)
			.type(testInfo.email)
			.should("have.value", testInfo.email);
		cy.get(fields.firstName)
			.type(testInfo.firstName)
			.should("have.value", testInfo.firstName);
		cy.get(fields.lastName)
			.type(testInfo.lastName)
			.should("have.value", testInfo.lastName);
		cy.get(fields.password)
			.type(testInfo.password)
			.should("have.value", testInfo.password);

		cy.selectBirthYear(
			testInfo.birthdate.day,
			testInfo.birthdate.month,
			testInfo.birthdate.year
		);
		cy.chooseGenderWear();
		cy.checkContactPrefs();

		cy.get(registerButton).click();

		cy.get("h1").should("contain", "Access Denied");
	});
});
