const mandatoryFieldsLocators = {
	email: "#Email",
	firstName: "#FirstName",
	lastName: "#LastName",
	password: "#Password",
	birthDay: "#BirthDay",
	birthMonth: "#BirthMonth",
	birthYear: "#BirthYear",
};

const optionalFieldsLocators = {
	genderWear: {
		women: ".qa-gender-female",
		men: ".qa-gender-male",
	},
	contactPrefs: "#preferences-checkboxlist-container label",
};

const registerButton = "#register";

const errors = {
	fieldErrorClass: "input-validation-error",
	errorMessage: "span.field-validation-error",
};

export {
	mandatoryFieldsLocators,
	optionalFieldsLocators,
	registerButton,
	errors,
};
