const validEmail = "fohaw28893@dukeoo.com";
const validFirstName = "Honey";
const validLastName = "Book";
const validPassword = "Aa12345678!";
const validBirthdateRange = {
	day: {
		min: 1,
		max: 28,
	},
	month: {
		min: 1,
		max: 12,
	},
	year: {
		min: 1910,
		max: 2004,
	},
};

const positiveTestInfo = {
	email: validEmail,
	firstName: validFirstName,
	lastName: validLastName,
	password: validPassword,
	birthdate: validBirthdateRange,
};

const negativeTestInfo = [
	{
		...positiveTestInfo,
		...{
			testName: "Empty Email",
			email: "",
			errorField: "email",
			errorMessage: "Oops! You need to type your email here",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Invalid Email",
			email: "Hello",
			errorField: "email",
			errorMessage: "Email fail! Please type in your correct email address",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Empty First Name",
			firstName: "",
			errorField: "firstName",
			errorMessage: "We need your first name – it’s nicer that way",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Invalid Characters First Name",
			firstName: 'H&ll"o.<>',
			errorField: "firstName",
			errorMessage: 'First name must not contain <, >, &, " or .',
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Empty Last Name",
			lastName: "",
			errorField: "lastName",
			errorMessage: "Last name, too, please!",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Invalid Characters Last Name",
			lastName: 'g&<>dbye."',
			errorField: "lastName",
			errorMessage: 'Last name must not contain <, >, &, " or .',
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Empty Password",
			password: "",
			errorField: "password",
			errorMessage: "Hey, we need a password here",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Long Password",
			password:
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
			errorField: "password",
			errorMessage: "Hold up, that's too long. Less than 100 characters please",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Short Password",
			password: "A12345678",
			errorField: "password",
			errorMessage: "Erm, you need 10 or more characters",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "No Birth Day",
			birthdate: {
				day: false,
				month: validBirthdateRange.month,
				year: validBirthdateRange.year,
			},
			errorField: "birthDay",
			errorMessage: "Enter your full date of birth",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "No Birth Month",
			birthdate: {
				day: validBirthdateRange.day,
				month: false,
				year: validBirthdateRange.year,
			},
			errorField: "birthMonth",
			errorMessage: "Enter your full date of birth",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "No Birth Year",
			birthdate: {
				day: validBirthdateRange.day,
				month: validBirthdateRange.month,
				year: false,
			},
			errorField: "birthYear",
			errorMessage: "Enter your full date of birth",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Invalid Date",
			birthdate: {
				day: 31,
				month: 2,
				year: validBirthdateRange.year,
			},
			errorField: "birthDay",
			errorMessage:
				"That doesn't look right… Add your date of birth to get a birthday treat!",
		},
	},
	{
		...positiveTestInfo,
		...{
			testName: "Too Young",
			birthdate: {
				day: 31,
				month: 12,
				year: 2005,
			},
			errorField: "birthDay",
			errorMessage: "Oops. Looks like you're too young to use ASOS.",
		},
	},
];

export { positiveTestInfo, negativeTestInfo };
